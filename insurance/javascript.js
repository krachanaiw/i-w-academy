$.validator.setDefaults({
    submitHandler: function () {
        alert("submitted!");
    }
});

    $(document).ready(function () {
        $("#form").validate({
            rules: {
                name: {
                    required: true,
                    minlength: 2
            
            },
            ssn:{
                required: true,
                    minlength: 9
                

            },
        spouse:{
                required: true,
                    minlength: 9
                },
             address:{
                required: true,
                    minlength: 2
                },
                 phone:{
                required: true,
                    length:10
                },
                email:{
                required: true,
                
                },
                companyname: {
                    required: true,
                    
            
            },


            },
            messages: {
                name: {
                    required: "Please enter a name",
                    minlength: "Your name must consist of at least 2 characters",

                },
                companyname: {
                    required: "Please enter a company name",
                    

                },
                ssn:{
                required: "please enter SS#",
                    minlength: "Your name must consist of at least 9 characters",
                

            },
            spouse:{
                required: "please enter Spouse's name",
                    minlength: "Your name must consist of at least 2 characters",
                },
                 address:{
                required: "please enter address",
                    minlength: "Your address must consist of at least 2 characters",
                },
                 phone:{
                required: "please enter phone number",
                    minlength: "phone number must consist 10 number",
                }


            },
            errorElement: "em",
            errorPlacement: function (error, element) {
                // Add the `help-block` class to the error element
                error.addClass("help-block");

                if (element.prop("type") === "checkbox") {
                    error.insertAfter(element.parent("label"));
                } else {
                    error.insertAfter(element);
                }
            },
            highlight: function (element, errorClass, validClass) {
                $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
            }
        });

      });