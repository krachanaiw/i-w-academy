

$(document).ready(function(){
    var nameRegex = /^\s*[a-zA-Z\s]+\s*$/;
    var addressReg = /^\s*[a-zA-Z0-9,\s]+\s*$/;
    var phoneReg = /^98\d{8}$/;
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    $("form").submit(function() {
        var name = $("#name").val();
        var address = $("#address").val();
        var phone = $("#phone").val();
        var email= $("#email").val();

        if(name=="" || !nameRegex.test(name)){
            alert('Name not valid');
            return false;
        }else if(address=="" || !addressReg.test(address)){
            alert('address not valid');
            return false;
        }else if(phone=="" || !phoneReg.test(phone)){
            alert('phone not valid');
            return false;
        }else if(email=="" || !emailReg.test(email)){
            alert('email not valid');
            return false;
        }else{
            alert('valid');
            return true;
        }                 
});
});